NAME = test_printf

SRC = main.c

OBJ = $(SRC:.c=.o)

CFLAGS += -Wextra -W -Werror -fno-builtin -Wno-format -Wno-format-extra-args
LDFLAGS += -L. -lftprintf

$(NAME):
	gcc $(CFLAGS) -o system_printf main.c
	gcc $(CFLAGS) -D_FT_USER__ -o user_printf main.c $(LDFLAGS)
	./system_printf > system.txt
	./user_printf > user.txt
	@colordiff user.txt system.txt --text && echo "\033[1;32mPRINTF OK\033[0m"|| echo "\033[1;31mYOU FAIL, CHECK THE DIFF OUTPUT, I AIN'T DOIN THE DIRTY WORK FOR YA\n\033[0m\033[1;33mTHERE MIGHT BE DIFFERENCES, BUT MAYBE IT'S STILL OKAY, DON'T PANIC\033[0m"

all: $(NAME)

clean:
	#rm -rf main.o

fclean: clean
	#rm -rf system_printf
	rm -rf user_printf

rebuild: distclean $(NAME)
